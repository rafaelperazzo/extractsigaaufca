# -*- coding: UTF-8 -*-
from comum import CONFIG_PATH,db,CSV_DIR,db_sqlite
import pandas as pd

c1_alunos_por_curso = """
SELECT c.nome as curso,count(d.id_discente) as quantidade_discentes 
FROM public.discente d
INNER JOIN public.curso c ON (d.id_curso=c.id_curso)
WHERE c.ativo=True and c.nivel='G' and d.status=1
GROUP BY d.id_curso,c.nome
"""

c2_alunos_por_unidade = """
SELECT u.nome as unidade,count(d.id_discente) as quantidade_discentes 
FROM public.discente d
INNER JOIN public.curso c ON (d.id_curso=c.id_curso)
INNER JOIN comum.unidade u ON (c.id_unidade=u.id_unidade)
WHERE c.ativo=True and d.status=1 and u.tipo_academica='5'
GROUP BY u.nome
ORDER BY u.nome
"""

c3_cargos = """
SELECT c.denominacao as cargo,count(DISTINCT s.id_pessoa) as quantidade 
FROM rh.servidor s
INNER JOIN rh.cargo c ON (s.id_cargo=c.id)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN comum.unidade u ON (s.id_unidade=u.id_unidade) 
WHERE s.id_situacao=1 and u.id_unidade!=605 and 
u.id_unidade!=56 and u.id_unidade!=61 and u.id_unidade!=63 and u.id_unidade!=59
GROUP BY c.denominacao
ORDER BY c.denominacao
"""

c4_carga_horaria_docente = """
SELECT u.nome as unidade,t.ano,t.periodo,sum(dt.ch_dedicada_periodo) as ch_total, count(DISTINCT p.nome) total_docentes,
sum(dt.ch_dedicada_periodo)/count(p.nome) as media
FROM ensino.docente_turma as dt
INNER JOIN rh.servidor s ON (dt.id_docente=s.id_servidor)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN ensino.turma t ON (dt.id_turma=t.id_turma)
INNER JOIN ensino.componente_curricular cc ON (t.id_disciplina=cc.id_disciplina)
INNER JOIN ensino.componente_curricular_detalhes ccd ON (cc.id_detalhe=ccd.id_componente_detalhes)
INNER JOIN comum.unidade u ON (s.id_unidade=u.id_unidade)
WHERE u.id_unidade!=58 and cc.nivel='G' and (t.periodo=1 or t.periodo=2) 
and (u.id_unidade) in (SELECT u.id_unidade FROM comum.unidade u WHERE tipo_academica = '5')
and t.id_situacao_turma in (1,2,3)
GROUP BY u.nome,t.ano,t.periodo
ORDER BY u.nome
"""

print("INICIANDO...")
df = pd.read_sql(c1_alunos_por_curso,db)
df.to_csv(CSV_DIR + 'alunos_curso.csv',sep=';',index=False)

df = pd.read_sql(c2_alunos_por_unidade,db)
df.to_csv(CSV_DIR + 'alunos_unidade.csv',sep=';',index=False)

df = pd.read_sql(c3_cargos,db)
df.to_csv(CSV_DIR + 'cargos.csv',sep=';',index=False)

df = pd.read_sql(c4_carga_horaria_docente,db)
df.to_csv(CSV_DIR + 'carga_horaria_evolucao_24.csv',sep=';',index=False)

print("FINALIZADO...")