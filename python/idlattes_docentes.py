# -*- coding: UTF-8 -*-
import pandas as pd
import zeep
import logging
import progressbar
import zipfile
import os
from comum import CONFIG_PATH,db,CSV_DIR,db_sqlite

CURRICULO_DIR = CONFIG_PATH + '/xml/'

logging.basicConfig(filename=CONFIG_PATH + '/error.log', filemode='w', format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=logging.ERROR)

docentes = """
SELECT p.nome, p.cpf_cnpj,u.nome as unidade,f.denominacao,s.regime_trabalho FROM comum.pessoa p 
INNER JOIN rh.servidor s ON (p.id_pessoa=s.id_pessoa)
INNER JOIN comum.unidade u ON (s.id_unidade=u.id_unidade) 
INNER JOIN rh.formacao f ON (s.id_formacao=f.id_formacao)
WHERE s.id_ativo=1 and s.id_categoria=1 and u.id_unidade!=605 and u.id_unidade!=56 and u.id_unidade!=61 and u.id_unidade!=63 and u.id_unidade!=59
ORDER BY u.nome,p.nome
"""

df_docentes = pd.read_sql(docentes,db)

##################################################
#OBTENDO IDENTIFICADOR LATTES
##################################################

wsdl = 'https://sci02-ter-jne.ufca.edu.br/cnpq'
client = zeep.Client(wsdl=wsdl)

def getID(cpf):
    idlattes = client.service.getIdentificadorCNPq(cpf,"","")
    return str(idlattes)

cpfs = list(df_docentes['cpf_cnpj'])

idslattes = []
i=0
with progressbar.ProgressBar(max_value=len(cpfs)) as bar:
    for cpf in cpfs:
        cpf_corrigido = str(cpf)
        if len(cpf_corrigido)<11:
            tamanho = 11-len(cpf_corrigido)
            cpf_corrigido = cpf_corrigido.zfill(11)
        idlattes = getID(cpf_corrigido)
        if idlattes is None:
            logging.error(cpf_corrigido + u" ------> IDLATTES NÃO ENCONTRADO")
            print(cpf_corrigido + u" ------> IDLATTES NÃO ENCONTRADO")
            idlattes.append(0)
        else:
            idslattes.append(idlattes)
        i = i + 1
        bar.update(i)

##################################################
#SALVANDO CSV DE DOCENTES
##################################################
df_docentes['idlattes'] = idslattes
df_docentes = df_docentes.drop(['cpf_cnpj'],axis=1)
df_docentes.to_csv(CSV_DIR + "01_docentes.csv",sep=';',index=False)
df_docentes.to_sql('docentes',db_sqlite)
##################################################
#SALVANDO CURRICULOS DE DOCENTES (XML)
##################################################

def salvarCV(id):
    resultado = client.service.getCurriculoCompactado(id)
    arquivo = open(id + '.zip','wb')
    arquivo.write(resultado)
    arquivo.close()
    with zipfile.ZipFile(id + '.zip','r') as zip_ref:
        zip_ref.extractall(CURRICULO_DIR)
    if os.path.exists(id + '.zip'):
        os.remove(id + '.zip')

i = 0
ids = idslattes
with progressbar.ProgressBar(max_value=len(ids)) as bar:
    for id in ids:
        resultado = client.service.getCurriculoCompactado(id)
        try:
            salvarCV(id)
        except Exception as e:
            logging.error(str(e))
        finally:
            i = i + 1
            bar.update(i)

