# -*- coding: UTF-8 -*-
import configparser
from sqlalchemy import create_engine
from criptografia import Anonimizador

config = configparser.ConfigParser()
CONFIG_PATH = "/python"
config.read(CONFIG_PATH + '/config.ini')
CSV_DIR = CONFIG_PATH + '/csv/'

HOSTNAME = config['postgresql']['host']
DATABASE = config['postgresql']['database']
USER = config['postgresql']['user']
PASSWORD = config['postgresql']['password']

STRING_CONEXAO = "postgresql://" + USER + ":" + PASSWORD + "@" + HOSTNAME + ":5432/" + DATABASE

db = create_engine(STRING_CONEXAO)
db_sqlite = create_engine("sqlite:////" + CONFIG_PATH + "/" + "sigaa.sqlite3")
cripto = Anonimizador()