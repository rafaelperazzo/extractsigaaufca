# -*- coding: UTF-8 -*-
from os import sep
import pandas as pd
import logging
from comum import CONFIG_PATH,db,db_sqlite
from criptografia import Anonimizador,AESCipher
from cryptography.fernet import Fernet
from Cryptodome.Cipher import AES
from Cryptodome import Random
from Cryptodome.Random import get_random_bytes

CURRICULO_DIR = CONFIG_PATH + '/xml/'

logging.basicConfig(filename=CONFIG_PATH + '/error.log', filemode='a', format='%(asctime)s %(name)s - %(levelname)s - %(message)s',level=logging.ERROR)

cpfs = """
SELECT DISTINCT p.cpf_cnpj
FROM ensino.docente_turma dt
LEFT JOIN avaliacao.resultado_avaliacao_docente rad ON (dt.id_docente_turma=rad.id_docente_turma)
INNER JOIN rh.servidor s ON (dt.id_docente=s.id_servidor)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN ensino.turma t ON (dt.id_turma=t.id_turma)
INNER JOIN ensino.componente_curricular cc ON (t.id_disciplina=cc.id_disciplina)
INNER JOIN ensino.componente_curricular_detalhes ccd ON (cc.id_detalhe=ccd.id_componente_detalhes)
WHERE t.id_situacao_turma in (1,2,3) AND cc.nivel='G'
AND t.periodo<3
"""

disciplinas = """
SELECT s.siape,p.nome as docente,t.ano,t.periodo,cc.codigo,ccd.nome as disciplina,
rad.media_geral,rad.desvio_padrao_geral,p.data_nascimento,p.cpf_cnpj
FROM ensino.docente_turma dt
LEFT JOIN avaliacao.resultado_avaliacao_docente rad ON (dt.id_docente_turma=rad.id_docente_turma)
INNER JOIN rh.servidor s ON (dt.id_docente=s.id_servidor)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN ensino.turma t ON (dt.id_turma=t.id_turma)
INNER JOIN ensino.componente_curricular cc ON (t.id_disciplina=cc.id_disciplina)
INNER JOIN ensino.componente_curricular_detalhes ccd ON (cc.id_detalhe=ccd.id_componente_detalhes)
WHERE t.id_situacao_turma in (1,2,3) AND
p.cpf_cnpj in 

(SELECT DISTINCT p.cpf_cnpj
FROM ensino.docente_turma dt
LEFT JOIN avaliacao.resultado_avaliacao_docente rad ON (dt.id_docente_turma=rad.id_docente_turma)
INNER JOIN rh.servidor s ON (dt.id_docente=s.id_servidor)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN ensino.turma t ON (dt.id_turma=t.id_turma)
INNER JOIN ensino.componente_curricular cc ON (t.id_disciplina=cc.id_disciplina)
INNER JOIN ensino.componente_curricular_detalhes ccd ON (cc.id_detalhe=ccd.id_componente_detalhes)
WHERE t.id_situacao_turma in (1,2,3) AND cc.nivel='G'
AND t.periodo<3) 

AND cc.nivel='G'
AND t.periodo<3
ORDER BY t.ano,t.periodo,p.nome
"""

'''
disciplinas = """
SELECT s.siape,p.nome as docente,t.ano,t.periodo,cc.codigo,ccd.nome as disciplina,
rad.media_geral,rad.desvio_padrao_geral,p.data_nascimento,p.cpf_cnpj
FROM ensino.docente_turma dt
LEFT JOIN avaliacao.resultado_avaliacao_docente rad ON (dt.id_docente_turma=rad.id_docente_turma)
INNER JOIN rh.servidor s ON (dt.id_docente=s.id_servidor)
INNER JOIN comum.pessoa p ON (s.id_pessoa=p.id_pessoa)
INNER JOIN ensino.turma t ON (dt.id_turma=t.id_turma)
INNER JOIN ensino.componente_curricular cc ON (t.id_disciplina=cc.id_disciplina)
INNER JOIN ensino.componente_curricular_detalhes ccd ON (cc.id_detalhe=ccd.id_componente_detalhes)
WHERE t.id_situacao_turma in (1,2,3) AND
s.id_ativo=1 AND cc.nivel='G'
AND t.periodo<3
ORDER BY t.ano,t.periodo,p.nome
"""
'''

periodo_letivo = """
SELECT ano,periodo FROM comum.calendario_academico where (now() 
between inicioperiodoletivo and fimperiodoletivo)
and nivel='G' and ativo=True and id_curso is null
and periodo<3
ORDER BY ano DESC, periodo DESC LIMIT 1
"""
periodo_letivo = """
SELECT ano,periodo FROM comum.calendario_academico where 
nivel='G' and ativo=True and id_curso is null
and periodo<3
ORDER BY ano DESC, periodo DESC LIMIT 1
"""

periodo_letivo = """
SELECT ano,periodo FROM comum.calendario_academico where 
nivel='G' and ativo=True and id_curso is null
and periodo<3
and iniciomatriculaonline is not null
ORDER BY ano DESC, periodo DESC LIMIT 1
"""

usuarios = """
SELECT p.nome, s.siape,p.cpf_cnpj
FROM comum.pessoa p 
INNER JOIN rh.servidor s ON (p.id_pessoa=s.id_pessoa)
INNER JOIN comum.unidade u ON (s.id_unidade=u.id_unidade) 
INNER JOIN rh.formacao f ON (s.id_formacao=f.id_formacao)
INNER JOIN rh.situacao_servidor ss ON (s.id_situacao=ss.id_situacao)
WHERE s.id_ativo=1 and s.id_categoria=1 and u.id_unidade!=605 and u.id_unidade!=56 and u.id_unidade!=61 
and u.id_unidade!=63 and u.id_unidade!=59
ORDER BY p.nome
"""

def gen_key():
    key = Fernet.generate_key()
    iv = Random.new().read(AES.block_size)
    #key = get_random_bytes(16)
    with open(CONFIG_PATH + "/secret.key","wb") as key_file:
        key_file.write(key)
    with open(CONFIG_PATH + "/secret.iv","wb") as key_file:
        key_file.write(iv)

def load_key():
    key = open(CONFIG_PATH + "/secret.key","rb").read()
    return (key)

def load_iv():
    iv = open(CONFIG_PATH + "/secret.iv","rb").read()
    return (iv)

def corrigirCPF(cpf):
    cpf_corrigido = str(cpf)
    if len(cpf_corrigido)<11:
        cpf_corrigido = cpf_corrigido.zfill(11)
    return (cpf_corrigido)

crypto = Anonimizador()
#gen_key()
chave = load_key()
iv = load_iv()
crypto2 = AESCipher(chave.decode(),iv)

df_disciplinas = pd.read_sql(disciplinas,db)
df_disciplinas['temp'] = df_disciplinas['docente'].astype(str) + df_disciplinas['ano'].astype(str) + df_disciplinas['periodo'].astype(str) + df_disciplinas['data_nascimento'].astype(str)
df_disciplinas['token'] = df_disciplinas['temp'].map(lambda x: crypto.genToken(str(x)))
df_disciplinas = df_disciplinas.drop(columns=['temp','data_nascimento'])
df_disciplinas['siape'] = df_disciplinas['siape'].map(lambda x: crypto2.encrypt(str(x)))
df_disciplinas['cpf_cnpj'] = df_disciplinas['cpf_cnpj'].astype(str).map(lambda x: corrigirCPF(x))
df_disciplinas['cpf_cnpj'] = df_disciplinas['cpf_cnpj'].map(lambda x: crypto2.encrypt(str(x)))
df_disciplinas['docente'] = df_disciplinas['docente'].map(lambda x: crypto2.encrypt2(str(x)))
df_disciplinas['disciplina'] = df_disciplinas['disciplina'].map(lambda x: crypto2.encrypt2(str(x)))
df_disciplinas['token'] = df_disciplinas['token'].map(lambda x: crypto2.encrypt(str(x)))
df_disciplinas['codigo'] = df_disciplinas['codigo'].map(lambda x: crypto2.encrypt2(str(x)))
df_disciplinas.to_sql('avaliacao',db_sqlite,if_exists='replace')

df_periodo_letivo = pd.read_sql(periodo_letivo,db)
df_periodo_letivo.to_sql('periodo_letivo',db_sqlite,if_exists='replace')


df_docentes = pd.read_csv(CONFIG_PATH + '/docentes_emails.csv',sep=';')
df_usuarios = pd.read_sql(usuarios,db)
df_usuarios['cpf'] = df_usuarios['cpf_cnpj'].astype(str)
df_usuarios['cpf'] = df_usuarios['cpf'].map(lambda x: corrigirCPF(x))
df_usuarios['cpf'] = df_usuarios['cpf'].map(lambda x: crypto2.encrypt(str(x)))
df_usuarios['password'] = df_usuarios['siape'].astype(str) + df_usuarios['cpf']
df_usuarios = df_usuarios.merge(df_docentes,how='left',on='nome')
df_usuarios['password'] = df_usuarios['password'].map(lambda x: crypto.hashSha1(x))
df_usuarios['nome'] = df_usuarios['nome'].map(lambda x: crypto2.encrypt2(x))
df_usuarios['siape'] = df_usuarios['siape'].map(lambda x: crypto2.encrypt2(str(x)))
df_usuarios['email'] = df_usuarios['email'].astype(str).map(lambda x: crypto2.encrypt2(x))
df_usuarios = df_usuarios.drop(columns=['cpf_cnpj','lastname'])
df_usuarios.to_sql('usuarios',db_sqlite,if_exists='replace')
