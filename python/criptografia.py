'''
Classe simples para anonimização de informações
    Copyright (C) 2021 Rafael Perazzo Barbosa Mota

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''
#gpg.encrypt("123456",recipients=None,symmetric='AES256',passphrase='12345',armor=True)
#base64.b64encode(str(e).encode())


from cryptography.fernet import Fernet
import hashlib
import base64
from Cryptodome import Random
from Cryptodome.Cipher import AES

#https://realpython.com/documenting-python-code/

class Anonimizador:
    
    """
    Uma classe para representar um objeto anonimizador

    Vários métodos para anonimizar e/ou mascarar um texto específico ou uma coluna de 
    um dataframe

    Atributos
    ----------
    Sem atributos

    Métodos
    -------
    anonymize(token)
        Encripta um texto com uma chave aleatória para posteriormente passar um hash sha1
        Sem possibilidade de volta

    
    Exemplo de uso
    --------------
        ==========================================================
        anon = Anonimizador()
        print(anon.hashBlake("RAFAEL PERAZZO"))

        df = pd.DataFrame([{'a':'a','b':'b'}, {'a':'a','b':'c'}])

        #Aplicando uma função a uma coluna do df
        df['a'] = df['a'].map(lambda x: anon.anonymize(str(x)))
        #df['b'] = df['b'].mask(df['b']!='','XXXXXXXXXXXX')
        df['b'] = anon.mask(df['b'],'XXXXXXXXX')
        #Selecionando a coluna 0
        print(df.iloc[:,[0]])
        print(df)
        ==========================================================

    """

    def anonymize(self,token):
        """Anonimiza o texto passado como parâmetro

        Parameters
        ----------
        token : str, obrigatório
            O texto a ser anonimizado
        """
        key = Fernet.generate_key()
        f = Fernet(key)
        encriptado = f.encrypt(str.encode(token))
        texto = hashlib.sha1(encriptado).hexdigest()
        return(texto)
    
    def encrypt(self,chave,texto):
        """Encripta o texto passado como parâmetro

        Parameters
        ----------
        chave: str, chave de criptografia

        texto : str, obrigatório
            O texto a ser encriptado
        
        Returns
        -------
        texto: str
           texto encriptado
        """
        f = Fernet(chave)
        encriptado = f.encrypt(str.encode(texto))
        return(encriptado.decode())

    def encrypt_gpg(self,chave,texto):
        """Encripta (com GPG) o texto passado como parâmetro

        Parameters
        ----------
        chave: str, chave de criptografia

        texto : str, obrigatório
            O texto a ser encriptado
        
        Returns
        -------
        texto: str
           texto encriptado
        """
        gpg = gnupg.GPG()
        encriptado = gpg.encrypt(texto,recipients=None,symmetric='AES256',passphrase=chave,armor=True)
        encriptado = base64.b64encode(str(encriptado).encode())
        return(encriptado)
        

    def genToken(self,texto):
        """Gera um hash sha1 do texto

        Parameters
        ----------
        
        texto : str, obrigatório
            O texto a ter seu hash
        
        Returns
        -------
        hash sha1 do texto
        """
        token = texto
        token = str.encode(token)
        texto_hash = hashlib.sha1(token).hexdigest()
        return(texto_hash)

    def hashSha1(self,token):
        """Passa um hash de sha1 no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash

        Returns
        -------
        sha1 do token
            assinatura sha1 do token
        """
        texto = hashlib.sha1(token.encode('utf8')).hexdigest()
        return(texto)
    
    def hashMd5(self,token):
        """Passa um hash de MD5 no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash

        Returns
        -------
        md5 do token
            assinatura sha1 do token
        """
        texto = hashlib.sha1(token.encode('utf8')).hexdigest()
        return(texto)

    def hashBlake(self,token,tamanho=24):
        """Passa um hash de Blake no texto

        Parameters
        ----------
        token : str
            O texto a ser passsado o hash
        
        tamanho : int , opcional

        Returns
        -------
        black do token
            assinatura blake do token
        """
        texto = hashlib.blake2b(token.encode('utf8'),digest_size=tamanho).hexdigest()
        return(texto)
    
    def mask_str(self,token,caractere,inicio,fim):
        """
        Mascara um texto qualquer

        Parameters
        ----------
        token: str
            O texto que será mascarado

        caractere : str 
            O caractere que será utilizado como máscara
        
        inicio: int
            Início de onde o texto será mascarado
        
        fim: int 
            Fim de onde o texto será mascarado

        Returns
        -------
        texto mascarado
            
        """
        if (inicio>=0) and (inicio<len(token)) and (fim>=0) and (fim<len(token)) and (fim>=inicio):
            prefixo = token[inicio:fim]
            tamanho = len(token)-len(prefixo)
            masked = prefixo + tamanho*caractere
            return masked
        else:
            return (len(token)*"*")
            
        

    def mask(self,series,text):
        """Mascara uma coluna de um dataframe pandas

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada
        
        texto : str 
            O texto que será utilizado como máscara

        Returns
        -------
        coluna do dataframe mascarada
            
        """
        return(series.mask(series!='',text))

    def maskCPF(self,series):
        """Mascara uma coluna de um dataframe pandas que represente um CPF

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (CPF)

        Returns
        -------
        coluna do CPF mascarada com o texto: XXX.XXX.XXX-XX
            
        """
        return(series.mask(series!='','XXX.XXX.XXX-XX'))
    
    def maskSIAPE(self,series):
        """Mascara uma coluna de um dataframe pandas que represente um SIAPE

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (SIAPE)

        Returns
        -------
        coluna do SIAPE mascarada com o texto: XXXXXXX
            
        """
        return(series.mask(series!='','XXXXXXX'))
    
    def maskNome(self,series):
        
        """Mascara uma coluna de um dataframe pandas que represente um Nome

        Parameters
        ----------
        series : Pandas Series
            A coluna do dataframe a ser mascarada (Nome)

        Returns
        -------
        coluna do Nome mascarada com o texto: NOME DA PESSOA - PROTEGIDO
            
        """

        return(series.mask(series!='','NOME DA PESSOA - PROTEGIDO'))


class AESCipher(object):

    def __init__(self, key,iv): 
        self.bs = AES.block_size
        self.key = hashlib.sha256(key.encode()).digest()
        self.iv = iv

    def encrypt(self, raw):
        raw = self._pad(raw)
        #iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CFB, self.iv)
        return (base64.b64encode(self.iv + cipher.encrypt(raw.encode()))).decode()

    def encrypt2(self, raw):
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        return (base64.b64encode(iv + cipher.encrypt(raw.encode()))).decode()

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        iv = enc[:AES.block_size]
        cipher = AES.new(self.key, AES.MODE_CFB, iv)
        return self._unpad(cipher.decrypt(enc[AES.block_size:])).decode('utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    @staticmethod
    def _unpad(s):
        return s[:-ord(s[len(s)-1:])]

'''
#EXEMPLOS
anon = Anonimizador()
print(anon.hashBlake("RAFAEL PERAZZO"))

df = pd.DataFrame([{'a':'a','b':'b'}, {'a':'a','b':'c'}])

#Aplicando uma função a uma coluna do df
df['a'] = df['a'].map(lambda x: anon.anonymize(str(x)))
#df['b'] = df['b'].mask(df['b']!='','XXXXXXXXXXXX')
df['b'] = anon.mask(df['b'],'XXXXXXXXX')
#Selecionando a coluna 0
print(df.iloc[:,[0]])
print(df)
'''


