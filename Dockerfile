FROM python:latest
WORKDIR /python
RUN pip install --no-cache-dir pandas numpy progress requests sqlalchemy psycopg2 zeep progressbar2 odfpy openpyxl lxml unidecode bs4 jinja2 geojson cryptography python-gnupg pycryptodomex
ENV TZ=America/Fortaleza
