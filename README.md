# Extract Sigaa UFCA
**aplicação para extrair dados do sigaa UFCA**
**calculo da pontuação lattes**

**Versão: 0.0.1**

## Requisitos

* [Docker](https://docs.docker.com/get-docker)
* [Docker-compose](https://docs.docker.com/compose/install/)

## Recusos

* Opção para anonimização de dados, através de máscara, criptografia e hash

## Entrada
* Base de dados mirror_sigaa no postgree
* Arquivo CSV com as colunas: idlattes,nome,area capes e unidade academica. Exemplo abaixo:

```
idlattes;nome;area;ua
9099116287225122;SERVIDOR 1;ENGENHARIAS I;CCT
5116712500151643;SERVIDOR 2;ARTES;IISCA
9245122165772401;SERVIDOR 3;CIENCIA_DA_COMPUTACAO; CCT
```

## Arquivo de configuração config.ini (modelo)

```
[postgresql]
host=db
database=mirror_sigaa
user=postgres
password=SUASENHA (definida em docker-compose.yml)
```

## Saída

* python/csv/
* output/resultados.csv

## Baixando o projeto

* Dentro de uma pasta onde serão armazenados os arquivo do projeto digite: 

```
git clone https://gitlab.com/rafaelperazzo/extractsigaaufca.git .
```

## Antes de executar

* Criar o arquivo config.ini em python/

## Como executar

```
docker-compose up -d (inicia o postgres e o adminer)
./run (executa python que acessa o postgres)
```
* Ver arquivo export/run para instruções de importação
* Acessar http://localhost:9001

## Passo a passo

* Inicie o adminer e o postgres
* Importe o database conforme /export/run
* Execure ./run

## Autor

* Prof. Rafael Perazzo Barbosa Mota ( rafael.mota (at) ufca.edu.br )

