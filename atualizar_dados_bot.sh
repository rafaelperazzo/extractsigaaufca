cd /home/perazzo/docker/projetos/postgres/
docker-compose run --rm python3 python /python/idlattes_docentes.py
cp /home/perazzo/docker/projetos/postgres/python/xml/*.xml /home/perazzo/docker/projetos/lattes/arquivos/xml/docentes/
cd /home/perazzo/docker/projetos/lattes/
docker-compose run --rm scorelattes
scp /home/perazzo/docker/projetos/lattes/arquivos/csv/docentes/UFCA_producao.csv sci01-ter-jne.ufca.edu.br:/dados/apps/bots/arquivos/
cd /home/perazzo/docker/projetos/postgres/
docker-compose run --rm python3 python /python/atualizar_dados_bot.py
scp /home/perazzo/docker/projetos/postgres/python/csv/*.csv sci01-ter-jne.ufca.edu.br:/dados/apps/bots/arquivos/

